# Finnish translation for ubuntu-terminal-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-terminal-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-terminal-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-20 09:02+0000\n"
"PO-Revision-Date: 2020-11-23 18:30+0000\n"
"Last-Translator: Jiri Grönroos <jiri.gronroos@iki.fi>\n"
"Language-Team: Finnish <https://translate.ubports.com/projects/ubports/"
"terminal-app/fi/>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2016-02-08 05:36+0000\n"

#: ../src/app/qml/AlternateActionPopover.qml:68
msgid "Select"
msgstr "Valitse"

#: ../src/app/qml/AlternateActionPopover.qml:73
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:176
msgid "Copy"
msgstr "Kopioi"

#: ../src/app/qml/AlternateActionPopover.qml:79
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:181
msgid "Paste"
msgstr "Liitä"

#: ../src/app/qml/AlternateActionPopover.qml:86
msgid "Split horizontally"
msgstr "Jaa vaakatasossa"

#: ../src/app/qml/AlternateActionPopover.qml:92
msgid "Split vertically"
msgstr "Jaa pystytasossa"

#: ../src/app/qml/AlternateActionPopover.qml:99
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:151
#: ../src/app/qml/TabsPage.qml:32
msgid "New tab"
msgstr "Uusi välilehti"

#: ../src/app/qml/AlternateActionPopover.qml:104
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:146
msgid "New window"
msgstr "Uusi ikkuna"

#: ../src/app/qml/AlternateActionPopover.qml:109
msgid "Close App"
msgstr "Sulje sovellus"

#: ../src/app/qml/AuthenticationDialog.qml:25
msgid "Authentication required"
msgstr "Tunnistautuminen vaaditaan"

#: ../src/app/qml/AuthenticationDialog.qml:27
msgid "Enter passcode or passphrase:"
msgstr "Anna suojakoodi tai salasana:"

#: ../src/app/qml/AuthenticationDialog.qml:48
msgid "passcode or passphrase"
msgstr "suojakoodi tai salasana"

#: ../src/app/qml/AuthenticationDialog.qml:58
msgid "Authenticate"
msgstr "Tunnistaudu"

#: ../src/app/qml/AuthenticationDialog.qml:70
#: ../src/app/qml/ConfirmationDialog.qml:42
msgid "Cancel"
msgstr "Peru"

#: ../src/app/qml/AuthenticationService.qml:64
msgid "Authentication failed"
msgstr "Tunnistautuminen epäonnistui"

#: ../src/app/qml/AuthenticationService.qml:83
msgid "No SSH server running."
msgstr "SSH-palvelin ei ole käynnissä."

#: ../src/app/qml/AuthenticationService.qml:84
msgid "SSH server not found. Do you want to proceed in confined mode?"
msgstr "SSH-palvelinta ei löytynyt. Haluatko jatkaa eristystilassa?"

#: ../src/app/qml/ConfirmationDialog.qml:32
msgid "Continue"
msgstr "Jatka"

#: ../src/app/qml/KeyboardBar.qml:197
msgid "Change Keyboard"
msgstr "Vaihda näppäimistöä"

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:35
msgid "Control Keys"
msgstr ""

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:38
msgid "Function Keys"
msgstr ""

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:41
msgid "Scroll Keys"
msgstr ""

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:44
msgid "Command Keys"
msgstr ""

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:52
msgid "Ctrl"
msgstr "Ctrl"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:55
msgid "Fn"
msgstr "Fn"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:58
msgid "Scr"
msgstr "Scr"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:61
msgid "Cmd"
msgstr "Cmd"

#. TRANSLATORS: This is the name of the Control key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:78
msgid "CTRL"
msgstr "CTRL"

#. TRANSLATORS: This is the name of the Alt key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:81
msgid "Alt"
msgstr "Alt"

#. TRANSLATORS: This is the name of the Shift key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:84
msgid "Shift"
msgstr "Shift"

#. TRANSLATORS: This is the name of the Escape key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:97
msgid "Esc"
msgstr "Esc"

#. TRANSLATORS: This is the name of the Page Up key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:100
msgid "PgUp"
msgstr "PgUp"

#. TRANSLATORS: This is the name of the Page Down key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:103
msgid "PgDn"
msgstr "PgDn"

#. TRANSLATORS: This is the name of the Delete key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:106
msgid "Del"
msgstr "Del"

#. TRANSLATORS: This is the name of the Home key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:109
msgid "Home"
msgstr "Home"

#. TRANSLATORS: This is the name of the End key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:112
msgid "End"
msgstr "End"

#. TRANSLATORS: This is the name of the Tab key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:115
msgid "Tab"
msgstr "Tab"

#. TRANSLATORS: This is the name of the Enter key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:118
msgid "Enter"
msgstr "Enter"

#: ../src/app/qml/NotifyDialog.qml:25
msgid "OK"
msgstr "Selvä"

#: ../src/app/qml/Settings/BackgroundOpacityEditor.qml:26
msgid "Background opacity:"
msgstr "Taustan peittokyky:"

#: ../src/app/qml/Settings/ColorPickerPopup.qml:79
msgid "R:"
msgstr "R:"

#: ../src/app/qml/Settings/ColorPickerPopup.qml:87
msgid "G:"
msgstr "G:"

#: ../src/app/qml/Settings/ColorPickerPopup.qml:95
msgid "B:"
msgstr "B:"

#: ../src/app/qml/Settings/ColorPickerPopup.qml:99
msgid "Undo"
msgstr "Kumoa"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:55
msgid "Text"
msgstr "Teksti"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:66
msgid "Font:"
msgstr "Fontti:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:91
msgid "Font Size:"
msgstr "Fontin koko:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:119
msgid "Colors"
msgstr "Värit"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:124
msgid "Ubuntu"
msgstr "Ubuntu"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:125
msgid "Green on black"
msgstr "Vihreä mustalla"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:126
msgid "White on black"
msgstr "Valkoinen mustalla"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:127
msgid "Black on white"
msgstr "Musta valkoisella"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:128
msgid "Black on random light"
msgstr "Musta satunnaisella vaalealla"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:129
msgid "Linux"
msgstr "Linux"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:130
msgid "Cool retro term"
msgstr "Siisti retro-pääte"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:131
msgid "Dark pastels"
msgstr "Tummat pastellit"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:132
msgid "Black on light yellow"
msgstr "Musta vaaleankeltaisella"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:133
msgid "Customized"
msgstr "Mukautettu"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:144
msgid "Background:"
msgstr "Tausta:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:152
msgid "Text:"
msgstr "Teksti:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:164
msgid "Normal palette:"
msgstr "Normaali paletti:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:171
msgid "Bright palette:"
msgstr "Kirkas paletti:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:180
msgid "Preset:"
msgstr "Esiasetus:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:197
msgid "Layouts"
msgstr "Asettelut"

#: ../src/app/qml/Settings/SettingsPage.qml:33
msgid "close"
msgstr "sulje"

#: ../src/app/qml/Settings/SettingsPage.qml:39
msgid "Preferences"
msgstr "Asetukset"

#: ../src/app/qml/Settings/SettingsPage.qml:50
msgid "Interface"
msgstr "Käyttöliittymä"

#: ../src/app/qml/Settings/SettingsPage.qml:54
msgid "Shortcuts"
msgstr "Pikanäppäimet"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:60
#, qt-format
msgid "Showing %1 of %2"
msgstr "Näytetään %1/%2"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:145
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:150
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:155
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:160
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:165
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:170
msgid "File"
msgstr "Tiedosto"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:156
msgid "Close terminal"
msgstr "Sulje pääte"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:161
msgid "Close all terminals"
msgstr "Sulje kaikki päätteet"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:166
msgid "Previous tab"
msgstr "Edellinen välilehti"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:171
msgid "Next tab"
msgstr "Seuraava välilehti"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:175
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:180
msgid "Edit"
msgstr "Muokkaa"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:185
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:190
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:195
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:200
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:205
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:210
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:215
msgid "View"
msgstr "Näytä"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:186
msgid "Toggle fullscreen"
msgstr "Koko näyttö päällä/pois"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:191
msgid "Split terminal horizontally"
msgstr "Jaa pääte vaakatasossa"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:196
msgid "Split terminal vertically"
msgstr "Jaa pääte pystytasossa"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:201
msgid "Navigate to terminal above"
msgstr "Siirry yläpuolella olevaan päätteeseen"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:206
msgid "Navigate to terminal below"
msgstr "Siirry alapuolella olevaan päätteeseen"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:211
msgid "Navigate to terminal on the left"
msgstr "Siirry vasemmalla olevaan päätteeseen"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:216
msgid "Navigate to terminal on the right"
msgstr "Siirry oikealla olevaan päätteeseen"

#: ../src/app/qml/Settings/SettingsWindow.qml:26
msgid "Terminal Preferences"
msgstr "Päätteen asetukset"

#: ../src/app/qml/Settings/ShortcutRow.qml:78
msgid "Enter shortcut…"
msgstr "Määritä pikanäppäin…"

#: ../src/app/qml/Settings/ShortcutRow.qml:80
msgid "Disabled"
msgstr "Pois käytöstä"

#: ../src/app/qml/TabsPage.qml:26
msgid "Tabs"
msgstr "Välilehdet"

#: ../src/app/qml/TerminalPage.qml:63 ubuntu-terminal-app.desktop.in.in.h:1
msgid "Terminal"
msgstr "Pääte"

#: ../src/app/qml/TerminalPage.qml:302
msgid "Selection Mode"
msgstr "Valintatila"

#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:291
#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:320
msgid "Un-named Color Scheme"
msgstr "Nimetön väriteema"

#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:468
msgid "Accessible Color Scheme"
msgstr "Esteetön väriteema"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:533
msgid "Open Link"
msgstr "Avaa linkki"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:534
msgid "Copy Link Address"
msgstr "Kopioi linkin osoite"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:538
msgid "Send Email To…"
msgstr "Lähetä sähköpostia…"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:539
msgid "Copy Email Address"
msgstr "Kopioi sähköpostiosoite"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:39
msgid "Match case"
msgstr "Huomioi kirjainkoko"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:45
msgid "Regular expression"
msgstr "Säännöllinen lauseke"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:49
msgid "Higlight all matches"
msgstr "Korosta kaikki osumat"

#: ../src/plugin/qmltermwidget/lib/Vt102Emulation.cpp:982
msgid ""
"No keyboard translator available.  The information needed to convert key "
"presses into characters to send to the terminal is missing."
msgstr ""
"Näppäimistökääntäjää ei ole saatavilla. Näppäinpainalluksia ei voida muuttaa "
"merkeiksi ja lähettää päätteelle."

#: ../src/plugin/qmltermwidget/lib/qtermwidget.cpp:406
msgid "Color Scheme Error"
msgstr "Väriteeman virhe"

#: ../src/plugin/qmltermwidget/lib/qtermwidget.cpp:407
#, qt-format
msgid "Cannot load color scheme: %1"
msgstr "Väriteeman lataaminen ei onnistu: %1"

#~ msgid "Color Scheme"
#~ msgstr "Väriteema"

#~ msgid "FNS"
#~ msgstr "FNS"

#~ msgid "SCR"
#~ msgstr "SCR"

#~ msgid "CMD"
#~ msgstr "CMD"

#~ msgid "ALT"
#~ msgstr "ALT"

#~ msgid "SHIFT"
#~ msgstr "SHIFT"

#~ msgid "ESC"
#~ msgstr "ESC"

#~ msgid "PG_UP"
#~ msgstr "PG_UP"

#~ msgid "PG_DN"
#~ msgstr "PG_DN"

#~ msgid "DEL"
#~ msgstr "DEL"

#~ msgid "HOME"
#~ msgstr "HOME"

#~ msgid "END"
#~ msgstr "END"

#~ msgid "TAB"
#~ msgstr "TAB"

#~ msgid "ENTER"
#~ msgstr "ENTER"

#~ msgid "Login"
#~ msgstr "Kirjaudu"

#~ msgid "Require login"
#~ msgstr "Vaadi kirjautuminen"

#~ msgid "Ok"
#~ msgstr "OK"

#~ msgid "Settings"
#~ msgstr "Asetukset"

#~ msgid "Required"
#~ msgstr "Vaadittu"

#~ msgid "Not required"
#~ msgstr "Ei vaadittu"

#~ msgid "Show Keyboard Bar"
#~ msgstr "Näytä näppäimistöpalkki"

#~ msgid "Enter password:"
#~ msgstr "Anna salasana:"

#~ msgid "password"
#~ msgstr "salasana"
